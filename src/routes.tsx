import * as React from "react";
import Loadable from "react-loadable";



//lazy loaded routes
function Loading({ error }) {
  if (error) {
    console.error(error);
    return "Oh nooess!";
  } else {
    return <div />;
  }
}

//important side note
//loadable loading expects component. not just plain jsx
//component must have default export.not nammed export

export const AppContainer = Loadable({
  loader: () => import("./containers/App/App"),
  loading: Loading
});
// user related pages
export const EditUserContainer = Loadable({
  loader: () => import("./containers/user/user"),
  loading: Loading
});
export const AddUserContainer = Loadable({
  loader: () => import("./containers/add-user/add-user"),
  loading: Loading
});



//login
export const LoginContainer = Loadable({
  loader: () => import("./containers/Login/Login"),
  loading: Loading
});
