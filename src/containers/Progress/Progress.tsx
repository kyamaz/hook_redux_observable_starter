import * as React from "react";
import styled from "styled-components";
import { AppState } from "@store/store.config";
import { connect } from "react-redux";
import { ProgressBar } from "src/Ui/progressBar";

const ProgressWrap = styled.section.attrs({
  className: "container"
})``;

interface ProgressProps {
  isLoading: boolean;
}
class appContainer extends React.Component<ProgressProps, null> {
  constructor(props: ProgressProps) {
    super(props);
  }

  public render(): JSX.Element {
    return (
      <ProgressWrap>
        <ProgressBar  dataRender={this.props.isLoading} />
      </ProgressWrap>
    );
  }
}

const mapStateToProps = ({ app }: AppState): { isLoading: boolean } => ({
  isLoading: app.isLoading
});

const ConnectedProgress = connect(
  mapStateToProps,
  null
)(appContainer);

export default ConnectedProgress;
