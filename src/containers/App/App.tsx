import * as React from "react";
import { AppState } from "@store/store.config";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { User } from "@model/index";
import { UserCard } from "../../components/user/user-card";
import { getUsersState } from "@store/select/user.select";
import { dispatchMaker } from "@store/dispatch/user.dispatch";
import { LOAD_USER } from "@store/actions";
import { ToEndColumns, SpaceAround, Grid } from "src/Ui/layout";
import { PrimaryBtn } from "src/Ui/btn";
import { AppTitle } from "src/Ui/title";

interface AppComponentProps {
  onLoadUsers: Function;
  users: Array<User>;
}
interface AppComponentState {}
class App extends React.Component<AppComponentProps, AppComponentState> {
  constructor(props: AppComponentProps) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.onLoadUsers();
  }

  componentDidUpdate(
    prevProps: AppComponentProps,
    prevState: AppComponentState
  ) {}

  public render(): JSX.Element {
    if (!Array.isArray(this.props.users)) {
      return <div />;
    }
    return (
      <Grid>
        <AppTitle>Redux Observable (partial) hook starter</AppTitle>

        <ToEndColumns>
          <PrimaryBtn>
            <Link to={`/user/add`}>Add user</Link>
          </PrimaryBtn>
        </ToEndColumns>

        <SpaceAround>
          {this.props.users.map(u => (
            <UserCard key={`user__${u.id}`} datum={u} />
          ))}
        </SpaceAround>
      </Grid>
    );
  }
}

const mapStateToProps = ({ users }: AppState): { users: Array<User> } => ({
  users: getUsersState(users)
});
const mapDispatchToProps = dispatch => ({
  onLoadUsers: (pagination: number) => dispatchMaker(LOAD_USER, 1)
});

const ConnectedProducts = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default ConnectedProducts;
