import * as React from "react";
import * as ReactDOM from "react-dom";
import { Fragment } from "react";
import registerServiceWorker from "./registerServiceWorker";
//css library
import "spectre.css/dist/spectre.min.css";
import "spectre.css/dist/spectre-icons.css";
import "./styles.css";
//style
import { GlobalStyle } from "@shared/style";

//redux
import { Provider } from "react-redux";
import configureStore from "./store/store.config";
export const store = configureStore();
//navigation
import { Route, Switch, Router, BrowserRouter } from "react-router-dom";
//routes
import {
  AddUserContainer,
  EditUserContainer,
  AppContainer,
  LoginContainer
} from "./routes";
import Progress from "@containers/Progress/Progress";
import ProtectedRoute from "./containers/ProtectedRoute/ProtectedRoute";
import { history } from "./route-history";

//JSX
// order is very important. its a switch case, so the most generic at bottom

function AppWrap(): JSX.Element {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Fragment>
          <GlobalStyle />
          <Progress />
          <Switch>
            <Route
              path="/login"
              render={props => <LoginContainer {...props} />}
            />
            <ProtectedRoute path="/" exact component={AppContainer} />
            <ProtectedRoute path="/user/add" component={AddUserContainer} />
            <ProtectedRoute path="/user/:id" component={EditUserContainer} />
            <ProtectedRoute component={AppContainer} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  );
}

const IndexComponent: JSX.Element = AppWrap();

ReactDOM.render(IndexComponent, document.getElementById("root") as HTMLElement);
registerServiceWorker();
