import { AuthPayload } from "@model/index";
import { LoginType, FormsControlsRef, InputState } from "./form";
import { SetStateAction, Dispatch } from "react";

export type UseDispatch<T> = Dispatch<SetStateAction<T>>;
export type UseHooks<T> = [T, UseDispatch<T>];

export type UseBoolean = UseHooks<boolean>;
export type UseLoginType = UseHooks<LoginType>;
export type UseAuthPayload = UseHooks<AuthPayload>;

// custom hooks
export interface UseLoginHook {
  loginInput: InputState;
  pwInput: InputState;
  confirmInput: InputState;
  isFormValid: boolean;
  inputsRef: FormsControlsRef;
  updateFormState: Function;
}
