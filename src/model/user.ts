export interface User{
    
        "id"?: number,
        "first_name":  string , 
        "last_name": string , 
        "avatar":  string , 
    
}

export interface UpdatedUser extends User {
        updatedAt:string
}