import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import Thunk from "redux-thunk";
import { reducer as formReducer } from "redux-form";
import { UsersReducerState, UserReducer } from "./reducers/user.reducer";
import { createEpicMiddleware, combineEpics } from "redux-observable";
import { loadUserEpic$ } from "./Epic/user.epic";
import { AppReducer, AppReducerState } from "./reducers/app.reducer";

const composeEnhancers =
  window["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] || compose;
//reducer
export interface AppState {
  form: any;
  users: UsersReducerState;
  app: AppReducerState;
}
const rootReducer = combineReducers({
  app: AppReducer,
  users: UserReducer,
  form: formReducer
});

//Epic

const rootEpic = combineEpics(loadUserEpic$);
const epicMiddleware = createEpicMiddleware();

const configureStore = () => {
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(epicMiddleware))
  );

  epicMiddleware.run(rootEpic);
  return store;
};
export default configureStore;
