import { IS_LOADING, SET_TOKEN } from "./../actions/index";
import { AppAction } from "./../../model/utils";

export interface AppReducerState {
  isLoading: boolean;
  token: string;
}
const initialAppState = {
  isLoading: false,
  token: null
};

const reducer = (
  state: AppReducerState = initialAppState,
  action: AppAction
): AppReducerState => {
  const { payload, type } = action;
  switch (type) {
    case IS_LOADING: {
      return { ...state, isLoading: payload };
    }
    case SET_TOKEN: {
      return { ...state, token: payload };
    }

    default:
      return state;
  }
};

export { reducer as AppReducer };
