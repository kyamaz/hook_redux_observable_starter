import { SET_TOKEN } from "./../actions/index";

import { store } from "src";
import { IS_LOADING } from "@store/actions";
export const dispatchIsLoading = (payload: boolean) =>
  store.dispatch({
    type: IS_LOADING,
    payload
  });
export const disptachAuthSuccess = (payload: string) => {
  store.dispatch({
    type: SET_TOKEN,
    payload
  });

  return payload;
};
