import { history } from "./../../route-history";
import { AUTH_FAIL } from "./index";
import { store } from "src";
import { disptachAuthSuccess } from "@store/dispatch/app.dispatch";

export const tryAuth = (payload: { login: string; pwd: string }) => {
  const { login, pwd } = payload;
  return new Promise((resolve, reject) => {
    if (login === "toto" && pwd === "rerere") {
      return resolve("superToken");
    }
    return reject("aut fail");
  })
    .catch(err => {
      store.dispatch({
        type: AUTH_FAIL
      });
    })
    .then((token: string) => disptachAuthSuccess(token))
    .then((token: string) => localStorage.setItem("hook_token", token))
    .then(res => history.push("/"));
};
