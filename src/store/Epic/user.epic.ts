import { actionMaker } from "./../dispatch/user.dispatch";
import { LOAD_USER, USER_ERROR, USER_LOADED } from "@store/actions";
import { DEMO_API } from "./../../shared/const";
import { ofType } from "redux-observable";
import { mergeMap, map, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { StoreAction, ApiResponse, User } from "@model/index";
import { of, Observable } from "rxjs";
// epic
//do not dispatch action
//use epic observable to update state
export const loadUserEpic$ = (
  action$: Observable<StoreAction<number>>
): Observable<StoreAction<any>> => {
  const url = `${DEMO_API}users?page=`;
  return action$.pipe(
    ofType(LOAD_USER),
    mergeMap((action: StoreAction<number>) => {
      const { payload } = action;
      return ajax.getJSON(`${url}${payload.toString()}`).pipe(
        catchError(err => of(actionMaker(USER_ERROR, err))),
        map((resp: ApiResponse<User>) => actionMaker(USER_LOADED, resp.data))
      );
    })
  );
};
