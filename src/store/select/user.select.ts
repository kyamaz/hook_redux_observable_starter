import { User } from "@model/index";
import { createSelector } from "reselect";
export const getUsersState = createSelector(
  [(userState: { users: Array<User> }) => userState.users],
  (users: Array<User>) => users
);
export const getUserState = createSelector(
  [(userState: { user: User }) => userState.user],
  (state: User) => {
    console.log(state);
    return state;
  }
);
export const getUserFirstName = createSelector(
  [(userState: { user: User }) => userState.user.first_name],
  (state: string) => {
    return state;
  }
);
