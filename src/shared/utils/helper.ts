export const formatArrToState = (
  arr: Array<Object>,
  ...path: Array<string>
): Object => {
  return arr.reduce((acc, curr) => {
    const key = safeGet<string>(curr, ...path);
    const newFormat = { [key]: curr };
    return { ...acc, ...newFormat };
  }, {});
};

//format id based object into an array
export const formatedStateToArr = (obj: Object): Array<Object> => {
  let arr: Array<Object> = [];
  for (let prop in obj) {
    arr = [...arr, ...[obj[prop]]];
  }
  return arr;
};
export function safeGet<T extends Object>(value: Object, ...path: string[]): T {
  return path.reduce((prev: Object, prop: string) => {
    if (prev && !!prev[prop]) {
      return prev[prop];
    } else {
      return null;
    }
  }, value);
}

export const normalizeStr = (str: string): string =>
  str
    .replace(/\s+/g, "")
    .trim()
    .toUpperCase();

//forms

export const isEmpty = (val: string): boolean =>
  val ? val.trim() === "" : true;
export const isValidUrl = (u: string): boolean =>
  u
    ? !!u.match(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
      )
    : false;

export function isEquivalent(a: Object, b: Object): boolean {
  // Create arrays of property names
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);

  // If number of properties is different,
  // objects are not equivalent
  if (aProps.length != bProps.length) {
    return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    const propName = aProps[i];

    // If values of same property are not equal,
    // objects are not equivalent
    if (a[propName] !== b[propName]) {
      return false;
    }
  }

  // If we made it this far, objects
  // are considered equivalent
  return true;
}
