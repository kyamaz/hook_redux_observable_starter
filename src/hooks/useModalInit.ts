import { useEffect } from "react";
import Modal from "react-modal";

export function useModalInit(el: any): void {
  useEffect(() => {
    Modal.setAppElement(el.current);
  }, []);
}
