import * as React from "react";
import styled from "styled-components";

export const Tabs = styled.ul.attrs({ className: "tab tab-block" })``;
export const TabItem = styled.ul.attrs({ className: "tab-item" })``;
