import * as React from "react";
import styled from "styled-components";
interface StyledProps {}

export const ListUnstyled = styled.ul<StyledProps>`
  list-style-type: none;
`;