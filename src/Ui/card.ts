import styled from "styled-components";

export const Card = styled.article.attrs({ className: "card" })``;
export const CardImage = styled.div.attrs({ className: "card-image" })``;
export const CardHeader = styled.header.attrs({ className: "card-header" })``;
export const CardBody = styled.section.attrs({ className: "card-body" })``;
export const CardFooter = styled.footer.attrs({ className: "card-footer" })``;
