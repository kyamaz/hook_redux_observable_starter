import * as React from "react";
import styled from "styled-components";
import { DEFAULT_COLOR } from "@shared/style";

const StyledProgressBar = styled.div.attrs({ className: "progress-bar" })<{}>`
  position: absolute;
  height: 5px;
  width: 100vw;
  margin: 0 -0.4rem;
`;
const Filler = styled.div.attrs({ className: "filler" })<{
  percentage: number;
  isLoading: boolean;
}>`
  width: ${props => props.percentage}%;
  background: ${props => (props.isLoading ? DEFAULT_COLOR : "transparent")};
  height: 100%;
  border-radius: inherit;
  transition: width 0.2s ease-in;
`;

export const ProgressBar = (props: {
  percentage?: number;
  dataRender?: boolean;
}) => {
  const percentage = !!props.dataRender ? 10 : 0;
  const [timer, setTimer] = React.useState(percentage);
  React.useEffect(
    () => {
      const t = !!props.dataRender ? timer + 55 : 0;
      setTimer(t)
    },

    [props.dataRender]
  );
  return (
    <StyledProgressBar>
      <Filler percentage={timer} isLoading={props.dataRender} />
    </StyledProgressBar>
  );
};
