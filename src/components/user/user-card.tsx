import * as React from "react";
import { User } from "@model/index";
import { Link } from "react-router-dom";
import { Card, CardImage, CardBody, CardFooter, CardHeader } from "src/Ui/card";
import { PrimaryBtn } from "src/Ui/btn";

interface Props {
  datum: User;
}
const user = (props: Props): JSX.Element => {
  return (
    <Card>
      <CardImage>
        <img src={props.datum.avatar} className="img-responsive" />
      </CardImage>
      <CardHeader>
        <div>
          {props.datum.first_name} {props.datum.last_name}{" "}
        </div>
      </CardHeader>
      <CardBody>
        <p>some description </p>
      </CardBody>
      <CardFooter>
        <PrimaryBtn>
          <Link to={`/user/${props.datum.id}`}>update</Link>
        </PrimaryBtn>
      </CardFooter>
    </Card>
  );
};

export { user as UserCard };
