import * as React from "react";
//state management
import { connect } from "react-redux";
import { User, ApiPayload } from "@model/index";

//interface

//comp
import { Field, reduxForm, InjectedFormProps, FormErrors } from "redux-form";
import { isEmpty, isValidUrl, safeGet } from "@shared/utils/helper";

import * as PropTypes from "prop-types";

export interface UserForm {
  first_name: string;
  last_name: string;
  avatar: string;
}
export interface UserField {
  name: string;
  label: string;
}
const fields: Array<UserField> = [
  { name: "first_name", label: "First name" },
  { name: "last_name", label: "Last name" },
  { name: "avatar", label: "Avatar" }
];

const renderFields = field => {
  return (
    <div>
      <div>
        <label>{field.label}</label>
      </div>
      <div>
        <input {...field.input} type="text" />
        <p>{field.meta.error}</p>
      </div>
    </div>
  );
};

const validate = (values): FormErrors => {
  let errors: { [prop: string]: string } = {};
  if (isEmpty(values.first_name)) {
    errors.first_name = "Enter a first name";
  }
  if (isEmpty(values.last_name)) {
    errors.last_name = "Enter a last name";
  }

  if (!isValidUrl(values.avatar)) {
    errors.avatar = "Enter a valid url";
  }
  return errors;
};

interface UserProps {
  handleSubmit?: any;
  initialValues: User;
  onUpdateUser: Function;
}

type UserFormProps = UserProps & InjectedFormProps<UserForm, UserProps>;

class UserFormComponent extends React.Component<UserFormProps, null> {
  constructor(props) {
    super(props);
  }
  static propTypes = {
    onUpdateUser: PropTypes.func
  };

  componentDidUpdate(prevProps: UserProps) {}
  private _submit = (datum: User) => this.props.onUpdateUser({ datum }); // console.log(datum)//this.props.onUpdateUser({ datum })
  //this.props.handleSubmit from redux form to prevent reload on submit

  public render(): JSX.Element {
    return (
      <form onSubmit={this.props.handleSubmit(this._submit)}>
        {fields.map((f: UserField, idx: number) => (
          <Field
            name={f.name}
            key={`${f.name}__${idx}`}
            label={f.label}
            component={renderFields}
          />
        ))}
        <button type="submit" disabled={this.props.invalid}>
          {safeGet(this.props, "initialValues", "id") ? "update" : "add"}
        </button>
      </form>
    );
  }
}

//to connext state to redux form
//form values must be initilazed props.initialValues
//then the fieldname should match prop name in form
//use the syntax below : first connect redux form to component  then to the store

//TODO properredux form typing
let UserEdit: any = reduxForm<any, any, any>({
  form: "editUserForm",
  validate,
  enableReinitialize: true
})(UserFormComponent);

UserEdit = connect(
  null,
  null
)(UserEdit);
export default UserEdit;
