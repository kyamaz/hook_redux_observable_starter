import * as React from "react";
import { HorizontalForm, FormHint } from "src/Ui/form";
import { FormInput } from "@components/FormInput/FormInput";
import { FormUpdatePayload } from "@model/index";
import { useLoginForm } from "@hooks/useLoginForm";
import { FormGroupLabeled } from "@components/FormGroupLabeled/FormGroupLabeled";
import { Columns } from "src/Ui/layout";
import { PrimaryBtn } from "src/Ui/btn";
import * as PropTypes from "prop-types";
import { uiValidation, displayErrorHint } from "@shared/utils/form";

interface LoginFormProps {
  loginType: string;
  tryAuth: Function;
  resetValidationError: Function;
  validState: boolean;
}

function loginForm(props: LoginFormProps): JSX.Element {
  const isSignup: boolean = props.loginType === "signup";
  const {
    loginInput,
    pwInput,
    confirmInput,
    isFormValid,
    inputsRef,
    updateFormState
  } = useLoginForm(isSignup);
  const onHandleUpdate = (f: FormUpdatePayload): void => {
    updateFormState(f, inputsRef);
    props.resetValidationError();
  };
  const _confirmLabel: JSX.Element = (
    <FormGroupLabeled
      label="confirm"
      isValid={uiValidation(inputsRef.confirmInput)}
    >
      <FormInput
        detail={{
          name: "confirmInput",
          type: "password",
          placeholder: "confirm password"
        }}
        value={confirmInput.value}
        handleUpdate={onHandleUpdate}
      />
      <FormHint>{displayErrorHint(inputsRef.confirmInput)}</FormHint>
    </FormGroupLabeled>
  );
  const confirm: JSX.Element = isSignup ? _confirmLabel : null;

  return (
    <HorizontalForm
      onSubmit={event => {
        event.preventDefault();
        props.tryAuth(inputsRef);
      }}
    >
      <FormGroupLabeled
        label="login"
        isValid={uiValidation(inputsRef.loginInput)}
      >
        <FormInput
          detail={{ name: "loginInput", type: "text", placeholder: "login" }}
          value={loginInput.value}
          handleUpdate={onHandleUpdate}
        />

        <FormHint>{displayErrorHint(inputsRef.loginInput)}</FormHint>
      </FormGroupLabeled>
      <FormGroupLabeled
        label="Password"
        isValid={uiValidation(inputsRef.pwInput)}
      >
        <FormInput
          detail={{
            name: "pwInput",
            type: "password",
            placeholder: "password"
          }}
          value={pwInput.value}
          handleUpdate={onHandleUpdate}
        />
        <FormHint>{displayErrorHint(inputsRef.pwInput)}</FormHint>
      </FormGroupLabeled>
      {confirm}
      <Columns>
        <PrimaryBtn isFormCta={true} disabled={!isFormValid} type="submit">
          save
        </PrimaryBtn>
        <FormHint>{props.validState ? null : "login failed"} </FormHint>
      </Columns>
    </HorizontalForm>
  );
}
loginForm.propTypes = {
  tryAuth: PropTypes.func,
  resetValidationError: PropTypes.func
};

export { loginForm as LoginForm };
